# lod-tools

Libraries for using Linked Open Data in project and components.

## Building and the dictionaries

Building the Linked Open Data dictionaries requires specifying a SPARQL server (using the `-Dexec.args`) that contains the following LOD sources:

 * dbpedia
 * wikidata
 * geonames

```bash
mvn clean compile deploy -Dexec.args="https://test.com/sparql"
```
