package net.semanticlab.lod.dictionary;

import java.util.List;
import java.util.Map;
import com.weblyzard.api.model.Lang;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum GeoDataSource implements PrecompiledDictionary {

    // @formatter:off 
    SWISS_COUNTRIES_REGIONS_CITIES_DE("swiss-cities", 
                    Map.of(Lang.ANY, 600, Lang.DE, 300), 
                    Map.of(Lang.DE, List.of("Chur", "Sargans", "Domat/Ems", 
                                    "Buchs SG"), Lang.ANY, List.of("Thalwil"))), 
    WORLD_COUNTRIES_DE("world-countries",
                    Map.of(Lang.DE, 100),
                    Map.of(Lang.DE, List.of("Schweiz", "Österreich","Australien"))), 
    WORLD_COUNTRIES_REGIONS_CITIES_DE("world-countries-cities-and-regions",
                    Map.of(Lang.ANY, 15000),
                    Map.of(Lang.ANY, List.of("Schweiz", "Canberra", "Bern", 
                                    "Graubünden", "Zug", "Florida"), 
                                    Lang.DE, List.of("Kärnten")));
    // @formatter:on 

    private static final List<Lang> SUPPORTED_LANGUAGES =
                    List.of(Lang.ANY, Lang.DE, Lang.EN, Lang.FR);

    @Getter
    private final String queryTemplate;
    @Getter
    private final Map<Lang, Integer> minDictionarySize;
    @Getter
    private final Map<Lang, List<String>> expectedEntries;

    @Override
    public List<Lang> getSupportedLanguages() {
        return SUPPORTED_LANGUAGES;
    }

}
