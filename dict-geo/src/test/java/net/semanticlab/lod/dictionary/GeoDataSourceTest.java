package net.semanticlab.lod.dictionary;

import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import com.weblyzard.api.model.Lang;

class GeoDataSourceTest {

    @Test
    void testEnumContent() {
        for (GeoDataSource g : GeoDataSource.values()) {
            for (Lang lang : g.getSupportedLanguages()) {
                int minDictionarySize = g.getMinDictionarySize().getOrDefault(lang, 0);
                assertTrue(g.getSet(lang).size() >= minDictionarySize);
            }
        }
    }

    @Test
    void testMultipleLanguages() {
        for (GeoDataSource g : GeoDataSource.values()) {
            int largestMinDictionarySize = g.getSupportedLanguages().stream()
                            .mapToInt(lang -> g.getMinDictionarySize().getOrDefault(lang, 0)).max()
                            .orElse(0);
            assertTrue(g.getSet(g.getSupportedLanguages().stream().toArray(Lang[]::new))
                            .size() >= largestMinDictionarySize);
        }
    }

}
