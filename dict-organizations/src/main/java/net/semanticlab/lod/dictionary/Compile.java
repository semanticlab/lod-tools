package net.semanticlab.lod.dictionary;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import net.semanticlab.lod.dictionary.helper.MultiLangSparqlDictionaryBuilder;

@Slf4j
public class Compile {

    public static void main(String[] argv) throws IOException {
        if (argv.length == 0) {
            String msg = "No SPARQL server specified. Please use the -Dexec.args option to specifiy a SPARQL server.";
            log.error(msg);
            throw new IllegalArgumentException(msg);
        }
        List<String> sparqlServers = Arrays.asList(argv[0].split(" "));
        log.info("Compiling enum '{}' with SPARQL servers: {}",
                        OrganizationDataSource.class.getName(), sparqlServers);

        MultiLangSparqlDictionaryBuilder.setSparqlServers(sparqlServers);
        for (OrganizationDataSource dictionary : OrganizationDataSource.values()) {
            MultiLangSparqlDictionaryBuilder.build(dictionary);
        }
    }

}
