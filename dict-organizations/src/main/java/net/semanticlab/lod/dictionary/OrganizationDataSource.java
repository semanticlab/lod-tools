package net.semanticlab.lod.dictionary;

import java.util.List;
import java.util.Map;
import com.weblyzard.api.model.Lang;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum OrganizationDataSource implements PrecompiledDictionary {

    // @formatter:off
    WIKIDATA_BRAND("wikidata-brand", 
                    Map.of(Lang.DE, 600, Lang.EN, 600, Lang.FR, 10),
                    Map.of(Lang.DE, List.of("3", "Xbox", "Pepsi"))), 
    WIKIDATA_ORGANIZATIONS("wikidata-organizations", 
                    Map.of(Lang.DE, 7000),
                    Map.of(Lang.DE, List.of("IBC Energie Wasser Chur", "REpower", "Credit Suisse", "UBS")));
    // @formatter:on

    private static final List<Lang> SUPPORTED_LANGUAGES =
                    List.of(Lang.ANY, Lang.DE, Lang.EN, Lang.FR);

    @Getter
    private final String queryTemplate;
    @Getter
    private final Map<Lang, Integer> minDictionarySize;
    @Getter
    private final Map<Lang, List<String>> expectedEntries;

    @Override
    public List<Lang> getSupportedLanguages() {
        return SUPPORTED_LANGUAGES;
    }

}
