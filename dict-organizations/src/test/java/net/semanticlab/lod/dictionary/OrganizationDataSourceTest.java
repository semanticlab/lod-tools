package net.semanticlab.lod.dictionary;

import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import com.weblyzard.api.model.Lang;

class OrganizationDataSourceTest {

    @Test
    void testEnumContent() {
        for (OrganizationDataSource g : OrganizationDataSource.values()) {
            for (Lang lang : g.getSupportedLanguages()) {
                int minDictionarySize = g.getMinDictionarySize().getOrDefault(lang, 0);
                assertTrue(g.getSet(lang).size() >= minDictionarySize);
            }
        }
    }

    @Test
    void testMultipleLanguages() {
        for (OrganizationDataSource g : OrganizationDataSource.values()) {
            int largestMinDictionarySize = g.getSupportedLanguages().stream()
                            .mapToInt(lang -> g.getMinDictionarySize().getOrDefault(lang, 0)).max()
                            .orElse(0);
            assertTrue(g.getSet(g.getSupportedLanguages().stream().toArray(Lang[]::new))
                            .size() >= largestMinDictionarySize);
        }
    }

}
