package net.semanticlab.lod.dictionary;

import java.util.Collection;
import java.util.List;

import net.semanticlab.lod.dictionary.datasource.DataSource;

public class DictionaryFactory {

    /**
     * Creates a dictionary of type &lt;T&gt; based on the data retrieved by the given
     * {@link DataSource}.
     * 
     * @return the number of entities retrieved from the dataSource
     */
    public static int createDictionary(Collection<String> dictionary, DataSource dataSource,
                    int minDictionarySize, List<String> expectedEntries) {

        // create dictionary
        while (dataSource.hasNext()) {
            dictionary.add(dataSource.next());
        }

        // validate the size of the created dictionary
        if (dictionary.size() < minDictionarySize) {
            throw new IllegalArgumentException(String.format(
                            "Dictionary size of %d is below the minimum exprected threshold of %s.",
                            dictionary.size(), minDictionarySize));
        }

        // validate the dictionary's entries
        expectedEntries.stream().filter(entry -> !dictionary.contains(entry)).findFirst()
                        .ifPresent(entry -> {
                            throw new IllegalArgumentException(String.format(
                                            "Dictionary is missing mandatory entry '%s'.", entry));
                        });
        return 0;
    }

}
