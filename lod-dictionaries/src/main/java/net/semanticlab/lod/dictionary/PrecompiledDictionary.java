package net.semanticlab.lod.dictionary;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.jena.ext.com.google.common.io.Resources;
import com.weblyzard.api.model.Lang;
import net.semanticlab.lod.dictionary.helper.MultiLangSparqlDictionaryBuilder;


public interface PrecompiledDictionary {

    static final String CACHED_DICTIONARY_PATH = "cached/";

    public String getQueryTemplate();

    public List<Lang> getSupportedLanguages();

    public Map<Lang, Integer> getMinDictionarySize();

    public Map<Lang, List<String>> getExpectedEntries();

    /**
     * Returns the path for the cached dictionaries and also creates the corresponding directory, if
     * required.
     */
    public default String getDictionaryFileName(Lang language) {
        return getQueryTemplate() + "_" + language.toString() + ".lexicion";
    }

    public default File getCachedDictionaryFile(Lang language) throws IOException {
        String path = getClass().getProtectionDomain().getCodeSource().getLocation().getFile()
                        + CACHED_DICTIONARY_PATH;
        Files.createDirectories(Paths.get(path));
        return new File(path + getDictionaryFileName(language));
    }

    public default URL getCachedDictionaryUrl(Lang language) {
        return Resources.getResource(CACHED_DICTIONARY_PATH + getDictionaryFileName(language));
    }

    public default Set<String> getSet(Lang... languages) {
        return MultiLangSparqlDictionaryBuilder.getSet(this, languages);
    }

}
