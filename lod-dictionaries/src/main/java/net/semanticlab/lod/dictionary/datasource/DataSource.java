package net.semanticlab.lod.dictionary.datasource;

import java.util.Iterator;

/**
 * A {@link DataSource} provides an iterator that yields entries retrieved from the underlying
 * adapter.
 * 
 * @author Albert Weichselbraun
 *
 */
public interface DataSource extends Iterator<String> {
}
