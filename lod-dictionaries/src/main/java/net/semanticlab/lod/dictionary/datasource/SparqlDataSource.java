package net.semanticlab.lod.dictionary.datasource;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.NoSuchElementException;
import org.apache.jena.graph.Node;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.weblyzard.sparql.StreamingQueryExecutor;
import com.weblyzard.sparql.StreamingResultSet;
import lombok.extern.slf4j.Slf4j;

/**
 * Provides results from a SPARQL server.
 * 
 * @author Albert Weichselbraun
 *
 */
@Slf4j
public class SparqlDataSource implements DataSource {

    private StreamingResultSet resultSet;

    /**
     * Creates a SPARQL data source by executing the corresponding queries.
     * 
     * @param sparqlServers a list of SPARQL servers to try.
     * @param queryTemplateName the SPARQL query template file
     * @param queryFilter optional filter conditions for filtering the result.
     */
    public SparqlDataSource(Collection<String> sparqlServers, String queryTemplateName,
                    String queryFilter) {
        try {
            URL queryTemplateUrl = Resources
                            .getResource("sparql-queries/" + queryTemplateName + ".sparql");
            String queryTemplate = Resources.toString(queryTemplateUrl, Charsets.UTF_8);
            if (!queryTemplate.contains("?name")) {
                String msg = "Invalid SPARQL template does not contain the result binding '?name'.";
                log.error(msg);
                throw new IllegalArgumentException(msg);
            }
            String query = String.format(queryTemplate, queryFilter);
            log.info("Executing query: {}", query);
            resultSet = executeQuery(sparqlServers, query);
        } catch (IOException | IllegalArgumentException e) {
            log.error("Cannot read SPARQL query from template file'{}': {}", queryTemplateName, e);
        }
    }

    /**
     * Executes a query on a SPARQL server.
     */
    private static StreamingResultSet executeQuery(Collection<String> sparqlServers, String query) {
        for (String sparqlServer : sparqlServers) {
            try {
                return StreamingQueryExecutor.getResultSet(sparqlServer, query);
            } catch (IOException e) {
                log.info("Skipping to next SPARQL server since query to '{}' failed: {}",
                                sparqlServer, e);
            }
        }
        String msg = "Cannot retrieve data from the specified SPARQL servers: "
                        + String.join(", ", sparqlServers);
        log.error(msg);
        throw new IllegalArgumentException(msg);
    }

    @Override
    public boolean hasNext() {
        return resultSet.hasNext();
    }

    @Override
    public String next() {
        try {
            Node result = resultSet.next().values().iterator().next();
            return result.isLiteral() ? result.getLiteral().toString() : "";
        } catch (NoSuchElementException e) {
            return "";
        }
    }

}
