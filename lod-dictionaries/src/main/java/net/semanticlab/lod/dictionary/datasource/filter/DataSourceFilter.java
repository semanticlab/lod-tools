package net.semanticlab.lod.dictionary.datasource.filter;

import net.semanticlab.lod.dictionary.datasource.DataSource;

public interface DataSourceFilter {

    /**
     * Returns a filter String for the given {@link DataSource}.
     */
    public String getFilter();

}
