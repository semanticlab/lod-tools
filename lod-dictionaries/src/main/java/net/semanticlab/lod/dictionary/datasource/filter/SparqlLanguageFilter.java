package net.semanticlab.lod.dictionary.datasource.filter;

import java.util.List;
import java.util.stream.Collectors;
import com.weblyzard.api.model.Lang;
import lombok.Getter;

public class SparqlLanguageFilter implements DataSourceFilter {

    @Getter
    private String filter;

    public SparqlLanguageFilter(List<Lang> validLanguages) {
        if (validLanguages.isEmpty()) {
            filter = "";
            return;
        }
        filter = validLanguages.stream()
                        .map(lang -> lang == Lang.ANY ? "lang(?name) = \"\""
                                        : String.format("lang(?name) = \"%s\"", lang.toString()))
                        .collect(Collectors.joining(" || ", "FILTER(", ")"));

    }

}
