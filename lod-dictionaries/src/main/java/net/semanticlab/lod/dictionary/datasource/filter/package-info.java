/**
 * Filters to use in conjunction with datasources.
 *
 * @author Albert Weichselbraun
 *
 */
package net.semanticlab.lod.dictionary.datasource.filter;
