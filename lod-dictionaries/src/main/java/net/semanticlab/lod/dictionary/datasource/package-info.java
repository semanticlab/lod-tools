/**
 * Provide datasources to use in conjunction with the LOD dictionary.
 *
 * @author Albert Weichselbraun
 *
 */
package net.semanticlab.lod.dictionary.datasource;
