package net.semanticlab.lod.dictionary.helper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.quinto.dawg.ModifiableDAWGSet;
import com.weblyzard.api.model.Lang;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.semanticlab.lod.dictionary.DictionaryFactory;
import net.semanticlab.lod.dictionary.PrecompiledDictionary;
import net.semanticlab.lod.dictionary.datasource.DataSource;
import net.semanticlab.lod.dictionary.datasource.SparqlDataSource;
import net.semanticlab.lod.dictionary.datasource.filter.SparqlLanguageFilter;

@Slf4j
public class MultiLangSparqlDictionaryBuilder {

    @Setter
    private static List<String> sparqlServers;

    private MultiLangSparqlDictionaryBuilder() {
    }

    /**
     * Compiles the lexicon and saves it in an efficient DAWG representation.
     */
    public static void build(PrecompiledDictionary dictionary) throws IOException {
        for (Lang language : dictionary.getSupportedLanguages()) {
            ModifiableDAWGSet lexicon = new ModifiableDAWGSet();
            DataSource dataSource = getDictionaryDataSource(dictionary, language);
            DictionaryFactory.createDictionary(lexicon, dataSource,
                            dictionary.getMinDictionarySize().getOrDefault(language, 0),
                            dictionary.getExpectedEntries().getOrDefault(language,
                                            Collections.emptyList()));
            log.info("Obtained a dictionary from query template '{}' and language '{}' with {} entries",
                            dictionary.getQueryTemplate(), language, lexicon.size());

            // save lexicon to file
            try (FileOutputStream fileOut =
                            new FileOutputStream(dictionary.getCachedDictionaryFile(language));
                            ObjectOutputStream objOut = new ObjectOutputStream(fileOut)) {
                objOut.writeObject(lexicon.compress());
            }
        }
    }


    /**
     * Returns a {@link Set} for the given language.
     */
    @SuppressWarnings("unchecked")
    public static Set<String> getSingularSet(PrecompiledDictionary dictionary, Lang language) {
        URL cacheFileUrl = dictionary.getCachedDictionaryUrl(language);
        try (ObjectInputStream objIn = new ObjectInputStream(cacheFileUrl.openStream())) {
            Set<String> result = (Set<String>) objIn.readObject();
            return result;
        } catch (IOException e) {
            log.error("Cannot open file: {}", e.toString());
            throw new NullPointerException(e.toString());
        } catch (ClassNotFoundException e) {
            log.error("Cannot cast object to Set: {}", e);
            throw new NullPointerException(e.toString());
        }
    }

    /**
     * Returns a {@link Set}that comprises the given array of languages.
     */
    public static Set<String> getSet(PrecompiledDictionary dictionary, Lang... languages) {
        if (languages.length == 0) {
            throw new IllegalArgumentException("No set language requested.");
        } else if (languages.length == 1) {
            return getSingularSet(dictionary, languages[0]);
        }
        ModifiableDAWGSet result = new ModifiableDAWGSet();
        for (int i = 0; i < languages.length; i++) {
            result.addAll(getSingularSet(dictionary, languages[i]));
        }
        return result;
    }

    private static DataSource getDictionaryDataSource(PrecompiledDictionary dictionary,
                    Lang language) {
        if (sparqlServers == null || sparqlServers.isEmpty()) {
            log.error("Cannot perform query, since no SPARQL servers have been set.");
            throw new NullPointerException("No SPARQL servers specified.");
        }
        return new SparqlDataSource(sparqlServers, dictionary.getQueryTemplate(),
                        new SparqlLanguageFilter(Collections.singletonList(language)).getFilter());
    }

}
