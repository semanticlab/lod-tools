/**
 * Helper classes for the LOD dictionary.
 *
 * @author Albert Weichselbraun
 *
 */
package net.semanticlab.lod.dictionary.helper;
