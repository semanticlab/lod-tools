/**
 * A factory that creates dictionaries based on a
 * {@link net.semanticlab.lod.dictionary.datasource.DataSource}.
 * 
 * @author Albert Weichselbraun
 *
 */
package net.semanticlab.lod.dictionary;
