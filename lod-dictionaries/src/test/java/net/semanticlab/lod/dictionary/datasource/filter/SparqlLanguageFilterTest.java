package net.semanticlab.lod.dictionary.datasource.filter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.weblyzard.api.model.Lang;

class SparqlLanguageFilterTest {

    @Test
    void testGetEmptyFilter() {
        DataSourceFilter lang = new SparqlLanguageFilter(Collections.emptyList());
        assertEquals("", lang.getFilter());
    }

    @Test
    void testAnyFilter() {
        DataSourceFilter lang = new SparqlLanguageFilter(List.of(Lang.DE, Lang.EN, Lang.ANY));
        assertEquals("FILTER(lang(?name) = \"de\" || lang(?name) = \"en\" || lang(?name) = \"\")",
                        lang.getFilter());
    }

}
